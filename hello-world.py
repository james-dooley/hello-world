#!/usr/bin/env python3

# --------------------------------------------------------------------------------------------------
# Title           : hello-world.py
# 
# Description     : 
# Author          : James Dooley
# Date            : 25.11.19
# Version         : 0.0.1
# Usage           : python3 hello-world.py
# Notes           :
# Python Version  : 3.7.0
#
# --------------------------------------------------------------------------------------------------

"""hello-world.py: """

print("hello world")